FROM python:alpine3.7
COPY test.py /test.py
RUN pip install requests
CMD python ./test.py
