import requests
import json
		
params = {}
params.update({'wstoken': "71e9f9491f6f46e18adb8edc6b9d6466", 'moodlewsrestformat': "json", 'wsfunction': "core_course_create_categories"})
params.update({'categories[0][name]': "KubernetesP4W4"})
params.update({'categories[0][parent]': str(int(0))})
params.update({'categories[0][idnumber]': "KKPW"})

# Se realiza la llamada al servicio
category = requests.post("http://campus.siatdi.net/webservice/rest/server.php", params).json()

response = {}

if type(category) == dict and category.get('exception'):
	print("Error al crear categoría:\n" + str(category))
else:
	print("Categoría creada:\n" + str(category))
